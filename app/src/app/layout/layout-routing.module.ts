import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { VisitorsComponent } from './visitors/visitors.component';
import { UsersComponent } from './users/users.component';
import { SettingsComponent } from './settings/settings.component';
import { ReportsComponent } from './reports/reports.component';
import {FormsModule} from '@angular/forms';
import { AuthGuard } from '../shared';
const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: 'visitors',component :VisitorsComponent,canActivate:[AuthGuard] },
            {path: 'users' , component :UsersComponent,canActivate:[AuthGuard]},
            {path: 'settings' ,component:SettingsComponent,canActivate:[AuthGuard]},
            {path : 'reports' ,component:ReportsComponent,canActivate:[AuthGuard]},
            
           
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes),FormsModule],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
