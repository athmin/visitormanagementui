import { Component, OnInit, ViewChild } from '@angular/core';
import {Subject} from 'rxjs';
import { Router } from '@angular/router';
import {ToasterService} from '../../toaster.service';
import {Observable} from 'rxjs';
import {FormControl,FormGroup,FormBuilder,Validators,FormArray, NgForm} from '@angular/forms';
import {WebcamImage, WebcamInitError, WebcamUtil} from 'ngx-webcam';
import { ServerService } from '../../server.service';

declare var $:any;
@Component({
  selector: 'app-visitors',
  templateUrl: './visitors.component.html',
  styleUrls: ['./visitors.component.scss']
})


export class VisitorsComponent implements OnInit {
  @ViewChild('mobile') mobileForm :NgForm
  @ViewChild('otp') otpForm :NgForm
  @ViewChild('basic') basicForm :NgForm
  // @ViewChild('covisitor') covisitorForm:NgForm
  // @ViewChild('additional') additional:NgForm
  visitorApiStart="api/entity/";
  mobileApiEnd="/visitor"
  optApiMid="/verifyotp/visitor/";
  basicDetailMid="/visitor/";

 
 checkinFlag=0;
  imageFlag=0;
  phoneFlag=1;
  otpFlag=0;
  basicFlag=0;
  covisitorFlag=0;
  additionalFlag=0;
  photoFlag=false;
 current_fs:any
 next_fs:any
 previous_fs:any //fieldsets
 left:any
 opacity:any
 scale:any; //fieldset properties which we will animate
 animating:any; //flag to prevent quick multi-click glitches
 
 
 public showWebcam = true;
 public allowCameraSwitch = true;
 public multipleWebcamsAvailable = false;
 public deviceId: string;
 public videoOptions: MediaTrackConstraints = {
   // width: {ideal: 1024},
   // height: {ideal: 576}
 };
 public errors: WebcamInitError[] = [];

 // latest snapshot
 public webcamImage: WebcamImage = null;

 // webcam snapshot trigger
 private trigger: Subject<void> = new Subject<void>();
 // switch to next / previous / specific webcam; true/false: forward/backwards, string: deviceId
 private nextWebcam: Subject<boolean|string> = new Subject<boolean|string>();


public myForm:FormGroup;


  constructor(private fb: FormBuilder,private servertransfer:ServerService,private toaster:ToasterService,public router: Router) {   }

   ngOnInit()  {
    
  this.myForm = this.fb.group({
    covisitors:this.fb.array([
      this.initCovisitor()])
  });

   
  }

  
onMobileSubmit() {
  console.log("mobile submit called..")
 var mobile_no:string=this.mobileForm.value.mobile_no;
 var mobileObj = {}
 mobileObj['mobile_no'] = mobile_no;
 var entity=localStorage.getItem('entity_id');
 var endUrl=this.visitorApiStart+ entity + this.mobileApiEnd;
this.servertransfer.push(endUrl,mobileObj)
.subscribe((data: any) => {
  if(data['status'] ==200){ 
    console.log("Succesfully hit phone API")
    var visitorid=data['result']['visitor_id']
    localStorage.setItem('visitor_id',visitorid);
 

}

})

}

onOtpSubmit(){
  console.log("otp submit is called..")
  var otpObj={}
  var otp=this.otpForm.value.otp1+this.otpForm.value.otp2+this.otpForm.value.otp3+this.otpForm.value.otp4
  console.log(otp)
  otpObj['otp']=otp

  var visitor_id=localStorage.getItem('visitor_id')
  var entity=localStorage.getItem('entity_id');
  var endUrl=this.visitorApiStart + entity + this.optApiMid +visitor_id
  console.log(endUrl)
  console.log(otpObj)
  this.servertransfer.push(endUrl,otpObj)
.subscribe((data: any) => {
  if(data['status'] ==200){ 
    console.log("Succesfully hit otp API")

}

})

}


onBasicDetailSubmit(){
  console.log("Basic Detail submit is called..")
  var fname:string=this.basicForm.value.first_name
  var gender:string=this.basicForm.value.gender
  var address:string=this.basicForm.value.address
  var city:string=this.basicForm.value.city
  var age:string=this.basicForm.value.age
  var basicDetailsObj={}
  basicDetailsObj['first_name']=fname
  // basicDetailsObj['last_name']='abc'
  // basicDetailsObj['middle_name']='efg'
  basicDetailsObj['gender']=gender
  basicDetailsObj['address']=address
  basicDetailsObj['city']=city
  // basicDetailsObj['pincode']='201910'
  basicDetailsObj['age_group']=age
  

  var visitor_id=localStorage.getItem('visitor_id')
  var entity=localStorage.getItem('entity_id');
  var endUrl=this.visitorApiStart + entity + this.basicDetailMid +visitor_id
  console.log(endUrl)
  console.log(basicDetailsObj)
  this.servertransfer.update(endUrl,basicDetailsObj)
.subscribe((data: any) => {
  if(data['status'] ==200){ 
    console.log("Succesfully hit Basic Detail API")

}

}) 
}



otpController(event,next,prev){
  if(event.target.value.length < 1 && prev){
    prev.focus()
    
  }
  else if(next && event.target.value.length>0){
    next.focus();
    
  }
  else {
   return 0;
  }
}

  initCovisitor(){
    return this.fb.group({
     name:[''],
     gender:['']
  });
  } 
  addCovisitor() {
    const control = <FormArray>this.myForm.controls['covisitors'];
    control.push(this.initCovisitor());
}

removeCovisitor(i: number) {
    const control = <FormArray>this.myForm.controls['covisitors'];
    control.removeAt(i);
}

  resetDivs()
  {
    this.phoneFlag=1;
    this.otpFlag=0;
    this.basicFlag=0;
    this.covisitorFlag=0;
    this.additionalFlag=0;
    this.photoFlag=false;
  }

 
  showDivs(btnid){
    console.log(btnid);
    if(btnid==11){
      console.log(true)
  this.otpFlag=1;
    }
    if(btnid==22){
      console.log(true)
  this.basicFlag=1;
    }
    if(btnid==33){
      console.log(true)
  this.covisitorFlag=1;
    }
    if(btnid==44){
      console.log(true)
   this.additionalFlag=1;
    }
    if(btnid==55){
      console.log(true)
  this.photoFlag=true;
    }


    
  }
 
  
  toggle(id)
  {
    if(id==1)
    {
    this.checkinFlag=1;
    }
    else if(id==2) 
    {
      this.checkinFlag=0;
    }


    
    this.phoneFlag=1;
    this.otpFlag=0;
    this.basicFlag=0;
    this.covisitorFlag=0;
    this.additionalFlag=0;
    this.photoFlag=false;
  }

  
  public getMedia():void{
  WebcamUtil.getAvailableVideoInputs()
  .then((mediaDevices: MediaDeviceInfo[]) => {
    this.multipleWebcamsAvailable = mediaDevices && mediaDevices.length > 1;
  });

}


  public triggerSnapshot(): void {
    this.trigger.next();
    this.imageFlag=1;
  }

  

  public toggleWebcam(): void {
    this.showWebcam = !this.showWebcam;
  }

  public handleInitError(error: WebcamInitError): void {
    this.errors.push(error);
  }

  public showNextWebcam(directionOrDeviceId: boolean|string): void {
    // true => move forward through devices
    // false => move backwards through devices
    // string => move to device with given deviceId
    this.nextWebcam.next(directionOrDeviceId);
  }

  public handleImage(webcamImage: WebcamImage): void {
    console.info('received webcam image', webcamImage);
    this.webcamImage = webcamImage;
  }

  public cameraWasSwitched(deviceId: string): void {
    console.log('active device: ' + deviceId);
    this.deviceId = deviceId;
  }

  public get triggerObservable(): Observable<void> {
    return this.trigger.asObservable();
  }

  public get nextWebcamObservable(): Observable<boolean|string> {
    return this.nextWebcam.asObservable();
  }


 


  

}
