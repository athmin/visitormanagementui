import { Component, OnInit,ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { NgForm } from '@angular/forms';
import {ToasterService} from '../toaster.service';
import {ServerService} from '../server.service';
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    
  @ViewChild('f') userForm: NgForm;
  loginApi = "api/user/sessions";


    constructor(public router: Router,public servertransfer:ServerService,public toaster:ToasterService) {}

    ngOnInit() {}

    onLoggedin() {
        //localStorage.setItem('isLoggedin', 'true');
        this.userForm.value.client_id = "xyz"
        console.log(this.userForm.value)
         var user_name:string=this.userForm.value.user_name;
         var password:string=this.userForm.value.password;
         var client_id:string=this.userForm.value.client_id;
         console.log(user_name)
        this.servertransfer.login(client_id,user_name,password,this.loginApi)
        .subscribe((data: any) => {
          var obj = data.header
          console.log(data['result']['user_id'])
          
          if(data['status'] ==200){ 
            this.toaster.Success("Succesfully logged in")
            this.userForm.reset()
            localStorage.setItem("isLoggedin","true");
            this.router.navigate(['/visitors']);
            var userid = data['result']['user_id']
            var entityid=data['result']['entity_id']
            var token=obj['x-auth-token']
            
           
          localStorage.setItem('user_id',userid);
          localStorage.setItem('entity_id',entityid);
          localStorage.setItem('x-auth-token',token);
        
        
        }
        })
   
    }
  
   




}
