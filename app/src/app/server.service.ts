import { Injectable } from '@angular/core';
import {Http,Headers,Response} from '@angular/http';
import {Router} from '@angular/router';
import {ToasterService} from './toaster.service';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/Rx';
import { LoginComponent } from './login/login.component';


@Injectable({
  providedIn: 'root'
})
export class ServerService {
  hostUrl = 'http://192.168.1.33:8090/'
  
  

  constructor(private http:Http,private toaster:ToasterService) { }
  


login(client_id, user_name, password,loginApi) {
  var body = {
    "client_id": client_id
  };
  var url = this.hostUrl + loginApi;
  console.log(user_name)
  var base64 = btoa(user_name + ":" + password)
  console.log(base64)
  var headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('Authorization', 'Basic ' + base64);
  var headerJson = { headers: headers };
  return this.http.post(url,
    body, headerJson).map(
      (response: Response) => {
        console.log(response.json());
       var x = response.headers.get('x-auth-token')
       console.log(x)

       var headerObj = {}
       headerObj['x-auth-token'] = x;
        return {result : response.json() , status : response.status , header : headerObj};
      }
    ).catch((error: any) => Observable.throw(this.toaster.Errors("Wrong Username Password")));
}




push(endUrl,body)
{
 
 
  var token=localStorage.getItem('x-auth-token');
  console.log(token)
  var url = this.hostUrl + endUrl
  console.log(url)
  var headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('x-auth-token', token);
  var headerJson = { headers: headers };
  return this.http.post(url,
    body, headerJson).map(
      (response: Response) => {
        console.log(response.json());
  
        return {result : response.json() , status : response.status};
      }
    ).catch((error: any) => Observable.throw(this.toaster.Errors("Invalid")));
}



update(endUrl,body)
{
 
 
  var token=localStorage.getItem('x-auth-token');
  console.log(token)
  var url = this.hostUrl + endUrl
  console.log(url)
  var headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('x-auth-token', token);
  var headerJson = { headers: headers };
  return this.http.put(url,
    body, headerJson).map(
      (response: Response) => {
        console.log(response.json());
  
        return {result : response.json() , status : response.status};
      }
    ).catch((error: any) => Observable.throw(this.toaster.Errors("Invalid")));
}



}
