import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
import { LoginComponent } from './login/login.component';
// import { VisitorsComponent } from './layout/visitors/visitors.component';
// import { UsersComponent } from './layout/users/users.component';
// import { SettingsComponent } from './layout/settings/settings.component';
// import { ReportsComponent } from './layout/reports/reports.component';

const routes: Routes = [
    { path: '', loadChildren: './layout/layout.module#LayoutModule' },
    { path: 'login', component:LoginComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
